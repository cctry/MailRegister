﻿/////////////////////////////////////////////////////////////
// 声明：本源码来自 vc驿站：http://www.cctry.com
// C、C++、VC++ 各种学习资源，免费教程，期待您的加入！
/////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RegThread.h"
#include "RegDlg.h"
#include "VCodeDlg.h"

#include "JsManager.h"
#pragma comment(lib, "./js185_static/lib/JsMgrStaticLib.lib")

#import "C:\\Windows\\System32\\WinHttp.dll" no_namespace

CString strMobileAgents = _T("Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Version/7.0 Mobile/11D257 Safari/9537.53");
CJsManager jsManager;

CString GetMidStrByLAndR(CString& strSrc, CString strLeft, CString strRight, BOOL bIncludeStart, BOOL bIncludeEnd)
{
	CString strRet;
	int eIdxBegin = strSrc.Find(strLeft);
	if(eIdxBegin != -1)
	{
		if (!bIncludeStart) {
			eIdxBegin += strLeft.GetLength();
		}

		int eIdxEnd = strSrc.Find(strRight, eIdxBegin+1);
		if (eIdxEnd != -1)
		{
			if (!bIncludeEnd) {
				strRet = strSrc.Mid(eIdxBegin, eIdxEnd-eIdxBegin);
			}else{
				eIdxEnd += strRight.GetLength();
				strRet = strSrc.Mid(eIdxBegin, eIdxEnd-eIdxBegin);
			}
		}
	}

	return strRet;
}

BOOL LoadMemImage(void *pMemData, ULONG nLen, CImage& imgObj)
{
	BOOL bRet = FALSE;
	HGLOBAL hGlobal = GlobalAlloc(GMEM_MOVEABLE, nLen);
	void *pData = GlobalLock(hGlobal);
	memcpy(pData, pMemData, nLen);
	GlobalUnlock(hGlobal);

	IStream *pStream = NULL;
	if(CreateStreamOnHGlobal(hGlobal, TRUE, &pStream) == S_OK) {
		HRESULT hr = imgObj.Load(pStream);
		if (SUCCEEDED(hr)) bRet = TRUE;
		pStream->Release();
	}
	GlobalFree(hGlobal);

	return bRet;
}

CString GetRandomString(UINT nLen)
{
	if (nLen <= 0) return _T("");

	CString retString;
	CString strSeeds = _T("abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ");
	UINT nSeedsLen = strSeeds.GetLength();
	srand((UINT)time(NULL));

	for (UINT idx = 0; idx < nLen; ++idx)
	{
		UINT seedIdx = rand()%nSeedsLen;
		retString += strSeeds[seedIdx];
	}

	return retString;
}

CString GetJSResBuffer()
{
	HMODULE hInstance = GetModuleHandle(NULL);
	HRSRC hRes = FindResource(hInstance, MAKEINTRESOURCE(IDR_JSCRIPT), _T("JSCRIPT"));
	if(NULL == hRes) return FALSE;

	HGLOBAL hgpt = LoadResource(hInstance, hRes);
	if (NULL == hgpt) return FALSE;

	DWORD rcSize = SizeofResource(hInstance, hRes);
	char *lpBuff = (char*)LockResource(hgpt);
	lpBuff[rcSize] = 0;

	CString strRet;
	strRet = lpBuff;

	return strRet;
}

BOOL GetEnvAndTimeString(CString& strEnv, CString& strTime)
{
	if (strEnv.IsEmpty()) return FALSE;

	jsval jsRet;
	CJsArray arrParam(&jsManager);
	arrParam.pushElement(strEnv);
	bool bRet = jsManager.evalFunction(_T("envFunc"), &arrParam, jsRet);
	if(!bRet) return FALSE;

	string outString;
	jsManager.jsval_to_string(jsRet, outString);
	strEnv = outString.c_str();

	bRet = jsManager.evalFunction(_T("getTimeString"), NULL, jsRet);
	if(!bRet) return FALSE;

	outString.clear();
	jsManager.jsval_to_string(jsRet, outString);
	strTime = outString.c_str();

	return TRUE;
}

BOOL GetJSessionIDInfo(IWinHttpRequestPtr& pHttpReq, CString& jSessionID, CString& strEnv, CString& strPrepareUrl)
{
	if(!pHttpReq) return FALSE;

	try {
		HRESULT hr = S_FALSE;

		hr = pHttpReq->Open(_T("GET"), _T("http://reg.email.163.com/unireg/call.do?cmd=register.entrance&flow=m_main&from=m_wapreg"));
		if(FAILED(hr)) return FALSE;

		pHttpReq->SetRequestHeader(_T("Accept"), _T("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"));
		pHttpReq->SetRequestHeader(_T("Accept-Language"), _T("zh-CN, en-US"));
		pHttpReq->SetRequestHeader(_T("User-Agent"), (LPCTSTR)strMobileAgents);
		pHttpReq->SetRequestHeader(_T("Host"), _T("reg.email.163.com"));
		pHttpReq->SetRequestHeader(_T("Connection"), _T("keep-alive"));
		//pHttpReq->SetRequestHeader(_T("Accept-Encoding"), _T("gzip, deflate"));

		hr = pHttpReq->Send();
		if(FAILED(hr)) return FALSE;

		_bstr_t bstrHeader = pHttpReq->GetAllResponseHeaders();

		long statusCode = pHttpReq->GetStatus();
		CString strContent;
		Get_WinHttp_RspString(pHttpReq, strContent);

		jSessionID = GetMidStrByLAndR(strContent, _T("sid : \""), _T("\""));
		if(jSessionID.IsEmpty()) return FALSE;

		strPrepareUrl = GetMidStrByLAndR(strContent, _T("<iframe id=\"submitFrame\" name=\"submitFrame\" style=\"display:none;\" src=\""), _T("\""));
		if(strPrepareUrl.IsEmpty()) return FALSE;

		strEnv = GetMidStrByLAndR(strContent, _T("envalue : \""), _T("\""));
		if(strEnv.IsEmpty()) return FALSE;
	}
	catch (...)
	{
		return FALSE;
	}

	return TRUE;
}

char* GetVerifyCodeBuffer(IWinHttpRequestPtr& pHttpReq, CString& sUrl, UINT& outLen)
{
	CString retString;
	if(!pHttpReq || sUrl.GetLength() <= 7) return NULL;

	long lRet = 0;
	HRESULT hr = S_FALSE;
	hr = pHttpReq->Open(_T("GET"), (LPCTSTR)sUrl);
	if(FAILED(hr)) return NULL;

	pHttpReq->SetRequestHeader(_T("Accept"), _T("*/*"));
	pHttpReq->SetRequestHeader(_T("Referer"), _T("http://reg.email.163.com/unireg/call.do?cmd=register.entrance&flow=m_main&from=m_wapreg"));
	pHttpReq->SetRequestHeader(_T("Accept-Language"), _T("zh-CN"));
	pHttpReq->SetRequestHeader(_T("User-Agent"), (LPCTSTR)strMobileAgents);
	pHttpReq->SetRequestHeader(_T("Host"), _T("reg.email.163.com"));
	pHttpReq->SetRequestHeader(_T("Connection"), _T("keep-alive"));

	hr = pHttpReq->Send();
	if(FAILED(hr)) return NULL;

	_variant_t varRspBody = pHttpReq->GetResponseBody();
	ULONG dataLen = varRspBody.parray->rgsabound[0].cElements;
	char *pContentBuffer = (char *)varRspBody.parray->pvData;
	if (dataLen <= 0 || !pContentBuffer) return NULL;

	_bstr_t cntType = pHttpReq->GetResponseHeader(_T("Content-Type"));
	_bstr_t imgType("image/jpeg");
	if (cntType != imgType) return NULL;

	char *pRetBuffer = new char[dataLen];
	CopyMemory(pRetBuffer, pContentBuffer, dataLen);
	outLen = dataLen;

	return pRetBuffer;
}

CImage* GetVerifyCode(IWinHttpRequestPtr& pHttpReq, CString& sUrl)
{
	if(!pHttpReq || sUrl.GetLength() <= 7) return NULL;

	UINT dataLen = 0;
	char *pContentBuffer = GetVerifyCodeBuffer(pHttpReq, sUrl, dataLen);
	_bstr_t cntType = pHttpReq->GetResponseHeader(_T("Content-Type"));
	_bstr_t imgType("image/jpeg");
	if (cntType != imgType) return NULL;

	CImage *pImg = new CImage();
	CImage &img = *pImg;
	BOOL bRet = LoadMemImage(pContentBuffer, dataLen, img);
	delete [] pContentBuffer;
	if (!bRet) return NULL;

	return pImg;
}

BOOL PreparePost(IWinHttpRequestPtr& pHttpReq, CString strUrl)
{
	HRESULT hr = S_FALSE;
	hr = pHttpReq->Open(_T("GET"), (LPCTSTR)strUrl);
	if(FAILED(hr)) return FALSE;

	pHttpReq->SetRequestHeader(_T("Accept"), _T("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"));
	pHttpReq->SetRequestHeader(_T("Referer"), _T("http://reg.email.163.com/unireg/call.do?cmd=register.entrance&flow=m_main&from=m_wapreg"));
	pHttpReq->SetRequestHeader(_T("Accept-Language"), _T("zh-CN, en-US"));
	pHttpReq->SetRequestHeader(_T("User-Agent"), (LPCTSTR)strMobileAgents);
	pHttpReq->SetRequestHeader(_T("Host"), _T("ssl.mail.163.com"));
	pHttpReq->SetRequestHeader(_T("Connection"), _T("keep-alive"));

	hr = pHttpReq->Send();
	if(FAILED(hr)) return FALSE;

	return TRUE;
}

BOOL CheckInfoPost(IWinHttpRequestPtr& pHttpReq, CString& strName, CString& strExt)
{
	if(!pHttpReq || strName.IsEmpty()) return FALSE;

	////////////////////// 1111111111111 //////////////////////
	CString strCheckUrl;
	strCheckUrl = _T("http://reg.email.163.com/unireg/call.do?cmd=urs.checkName");

	HRESULT hr = S_FALSE;
	hr = pHttpReq->Open(_T("POST"), (LPCTSTR)strCheckUrl);
	if(FAILED(hr)) return FALSE;

	pHttpReq->SetRequestHeader(_T("Accept"), _T("*/*"));
	pHttpReq->SetRequestHeader(_T("Content-Type"), _T("application/x-www-form-urlencoded; charset=UTF-8"));
	pHttpReq->SetRequestHeader(_T("X-Requested-With"), _T("XMLHttpRequest"));
	pHttpReq->SetRequestHeader(_T("Referer"), _T("http://reg.email.163.com/unireg/call.do?cmd=register.entrance&flow=m_main&from=m_wapreg"));
	pHttpReq->SetRequestHeader(_T("Accept-Language"), _T("zh-CN, en-US"));
	//pHttpReq->SetRequestHeader(_T("Accept-Encoding"), _T("gzip, deflate"));
	pHttpReq->SetRequestHeader(_T("User-Agent"), (LPCTSTR)strMobileAgents);
	pHttpReq->SetRequestHeader(_T("Host"), _T("reg.email.163.com"));

	pHttpReq->SetRequestHeader(_T("Connection"), _T("keep-alive"));
	pHttpReq->SetRequestHeader(_T("Origin"), _T("http://reg.email.163.com"));

	CString strBody;
	strBody.Format(_T("name=%s"), strName);
	pHttpReq->Send((LPCTSTR)strBody);

	//////////////////// 22222222222222222 ///////////////////////
	strCheckUrl = _T("http://reg.email.163.com/unireg/call.do?cmd=register.formLog");
	hr = pHttpReq->Open(_T("POST"), (LPCTSTR)strCheckUrl);
	if(FAILED(hr)) return FALSE;

	pHttpReq->SetRequestHeader(_T("Accept"), _T("*/*"));
	pHttpReq->SetRequestHeader(_T("Content-Type"), _T("application/x-www-form-urlencoded; charset=UTF-8"));
	pHttpReq->SetRequestHeader(_T("X-Requested-With"), _T("XMLHttpRequest"));
	pHttpReq->SetRequestHeader(_T("Referer"), _T("http://reg.email.163.com/unireg/call.do?cmd=register.entrance&flow=m_main&from=m_wapreg"));
	pHttpReq->SetRequestHeader(_T("Accept-Language"), _T("zh-CN, en-US"));
	//pHttpReq->SetRequestHeader(_T("Accept-Encoding"), _T("gzip, deflate"));
	pHttpReq->SetRequestHeader(_T("User-Agent"), (LPCTSTR)strMobileAgents);
	pHttpReq->SetRequestHeader(_T("Host"), _T("reg.email.163.com"));
	pHttpReq->SetRequestHeader(_T("Connection"), _T("keep-alive"));
	pHttpReq->SetRequestHeader(_T("Origin"), _T("http://reg.email.163.com"));

	strBody.Empty();
	strBody.Format(_T("opt=write_field&flow=m_main&field=name&result=done&uid=%s&timecost=8675&level=info"), strName+strExt);
	pHttpReq->Send((LPCTSTR)strBody);

	Sleep(50);
	//////////////////// 33333333333333 ///////////////////////
	strCheckUrl = _T("http://reg.email.163.com/unireg/call.do?cmd=register.formLog");
	hr = pHttpReq->Open(_T("POST"), (LPCTSTR)strCheckUrl);
	if(FAILED(hr)) return FALSE;

	pHttpReq->SetRequestHeader(_T("Accept"), _T("*/*"));
	pHttpReq->SetRequestHeader(_T("Content-Type"), _T("application/x-www-form-urlencoded; charset=UTF-8"));
	pHttpReq->SetRequestHeader(_T("X-Requested-With"), _T("XMLHttpRequest"));
	pHttpReq->SetRequestHeader(_T("Referer"), _T("http://reg.email.163.com/unireg/call.do?cmd=register.entrance&flow=m_main&from=m_wapreg"));
	pHttpReq->SetRequestHeader(_T("Accept-Language"), _T("zh-CN, en-US"));
	//pHttpReq->SetRequestHeader(_T("Accept-Encoding"), _T("gzip, deflate"));
	pHttpReq->SetRequestHeader(_T("User-Agent"), (LPCTSTR)strMobileAgents);
	pHttpReq->SetRequestHeader(_T("Host"), _T("reg.email.163.com"));
	pHttpReq->SetRequestHeader(_T("Connection"), _T("keep-alive"));
	pHttpReq->SetRequestHeader(_T("Origin"), _T("http://reg.email.163.com"));

	//_variant_t varRet;
	//CJsArray arrParam;
	//arrParam.pushElement(_T("password123"));
	//bool bRet = jsManager.evalFunction(_T("checkPasswordStrength"), &arrParam, varRet);
	//if(!bRet) return FALSE;
	//CString strength = varRet;

	strBody.Empty();
	strBody.Format(_T("opt=write_field&flow=m_main&field=pwd&result=done&strength=%s&timecost=5686&level=info"), _T("2"));
	pHttpReq->Send((LPCTSTR)strBody);

	Sleep(50);

	//////////////////// 4444444444444444 ///////////////////////
	strCheckUrl = _T("http://reg.email.163.com/unireg/call.do?cmd=register.formLog");
	hr = pHttpReq->Open(_T("POST"), (LPCTSTR)strCheckUrl);
	if(FAILED(hr)) return FALSE;

	pHttpReq->SetRequestHeader(_T("Accept"), _T("*/*"));
	pHttpReq->SetRequestHeader(_T("Content-Type"), _T("application/x-www-form-urlencoded; charset=UTF-8"));
	pHttpReq->SetRequestHeader(_T("X-Requested-With"), _T("XMLHttpRequest"));
	pHttpReq->SetRequestHeader(_T("Referer"), _T("http://reg.email.163.com/unireg/call.do?cmd=register.entrance&flow=m_main&from=m_wapreg"));
	pHttpReq->SetRequestHeader(_T("Accept-Language"), _T("zh-CN, en-US"));
	//pHttpReq->SetRequestHeader(_T("Accept-Encoding"), _T("gzip, deflate"));
	pHttpReq->SetRequestHeader(_T("User-Agent"), (LPCTSTR)strMobileAgents);
	pHttpReq->SetRequestHeader(_T("Host"), _T("reg.email.163.com"));
	pHttpReq->SetRequestHeader(_T("Connection"), _T("keep-alive"));
	pHttpReq->SetRequestHeader(_T("Origin"), _T("http://reg.email.163.com"));

	strBody.Empty();
	strBody = _T("opt=write_field&flow=m_main&field=vcode&result=done&timecost=6658&level=info");
	pHttpReq->Send((LPCTSTR)strBody);

	return TRUE; // for mobile
}

CImage *GetVCode1ImageByUrl(IWinHttpRequestPtr& pHttpReq, CString sUrl)
{
	if(!pHttpReq || sUrl.GetLength() <= 7) return NULL;

	long lRet = 0;
	HRESULT hr = S_FALSE;
	hr = pHttpReq->Open(_T("GET"), (LPCTSTR)sUrl);
	if(FAILED(hr)) return NULL;

	pHttpReq->SetRequestHeader(_T("Accept"), _T("*/*"));
	pHttpReq->SetRequestHeader(_T("Referer"), _T("http://reg.email.163.com/unireg/call.do?cmd=register.entrance&flow=m_main&from=m_wapreg"));
	pHttpReq->SetRequestHeader(_T("Accept-Language"), _T("zh-CN"));
	pHttpReq->SetRequestHeader(_T("User-Agent"), (LPCTSTR)strMobileAgents);
	pHttpReq->SetRequestHeader(_T("Host"), _T("reg.email.163.com"));
	pHttpReq->SetRequestHeader(_T("Connection"), _T("keep-alive"));

	hr = pHttpReq->Send();
	if(FAILED(hr)) return NULL;

	_variant_t varRspBody = pHttpReq->GetResponseBody();
	ULONG dataLen = varRspBody.parray->rgsabound[0].cElements;
	char *pContentBuffer = (char *)varRspBody.parray->pvData;
	if (dataLen <= 0 || !pContentBuffer) return NULL;
	_bstr_t cntType = pHttpReq->GetResponseHeader(_T("Content-Type"));
	_bstr_t imgType("image/jpeg");
	if (cntType != imgType) return NULL;

	CImage *pImg = new CImage();
	CImage &img = *pImg;
	BOOL bRet = LoadMemImage(pContentBuffer, dataLen, img);
	if (!bRet) return NULL;

	return pImg;
}

char *GetVCode1BufferByEnv(IWinHttpRequestPtr& pHttpReq, CString strEnv, UINT& outLen)
{
	if(!pHttpReq || strEnv.IsEmpty()) return NULL;

	CString strTime;
	BOOL bRet = GetEnvAndTimeString(strEnv, strTime);
	if (!bRet) return NULL;

	CString strVCodeUrl;
	strVCodeUrl.Format(_T("http://reg.email.163.com/unireg/call.do?cmd=register.verifyCode&v=common/verifycode/vc_en&env=%s&t=%s"), strEnv, strTime);

	HRESULT hr = S_FALSE;
	hr = pHttpReq->Open(_T("GET"), (LPCTSTR)strVCodeUrl);
	if(FAILED(hr)) return NULL;

	pHttpReq->SetRequestHeader(_T("Accept"), _T("*/*"));
	pHttpReq->SetRequestHeader(_T("Referer"), _T("http://reg.email.163.com/unireg/call.do?cmd=register.entrance&flow=m_main&from=m_wapreg"));
	pHttpReq->SetRequestHeader(_T("Accept-Language"), _T("zh-CN"));
	pHttpReq->SetRequestHeader(_T("User-Agent"), (LPCTSTR)strMobileAgents);
	pHttpReq->SetRequestHeader(_T("Host"), _T("reg.email.163.com"));
	pHttpReq->SetRequestHeader(_T("Connection"), _T("keep-alive"));

	hr = pHttpReq->Send();
	if(FAILED(hr)) return NULL;

	_variant_t varRspBody = pHttpReq->GetResponseBody();
	ULONG dataLen = varRspBody.parray->rgsabound[0].cElements;
	char *pContentBuffer = (char *)varRspBody.parray->pvData;
	if (dataLen <= 0 || !pContentBuffer) return NULL;
	_bstr_t cntType = pHttpReq->GetResponseHeader(_T("Content-Type"));
	_bstr_t imgType("image/jpeg");
	if (cntType != imgType) return NULL;

	char *pReBuffer = new char[dataLen];
	CopyMemory(pReBuffer, pContentBuffer, dataLen);
	outLen = dataLen;

	return pReBuffer;
}

CImage *GetVCode1ImageByEnv(IWinHttpRequestPtr& pHttpReq, CString strEnv)
{
	if(!pHttpReq || strEnv.IsEmpty()) return NULL;

	UINT dataLen = 0;
	char *pContentBuffer = GetVCode1BufferByEnv(pHttpReq, strEnv, dataLen);
	if (!pContentBuffer) return NULL;

	CImage *pImg = new CImage();
	CImage &img = *pImg;
	BOOL bRet = LoadMemImage(pContentBuffer, dataLen, img);
	delete [] pContentBuffer;
	if (!bRet) return NULL;

	return pImg;
}

CString GetVCode1(IWinHttpRequestPtr& pHttpReq, CString strEnv, HWND hParent)
{
	CString strVCode;

	if (hParent && IsWindow(hParent)) {
		CImage *pImg = GetVCode1ImageByEnv(pHttpReq, strEnv);
		if (!pImg) return _T("");

		CVCodeDlg vcodeDlg(CWnd::FromHandle(hParent));
		vcodeDlg.m_pImage = pImg;
		INT_PTR nRet = vcodeDlg.DoModal();
		if (nRet != IDOK) return _T("####");
		strVCode = vcodeDlg.m_strVCode;
	}else{
		CString strTime;
		BOOL bRet = GetEnvAndTimeString(strEnv, strTime);
		if (!bRet) return _T("");

		CString strVCodeUrl;
		strVCodeUrl.Format(_T("http://reg.email.163.com/unireg/call.do?cmd=register.verifyCode&v=common/verifycode/vc_en&env=%s&t=%s"), strEnv, strTime);

		HRESULT hr = S_FALSE;
		hr = pHttpReq->Open(_T("GET"), (LPCTSTR)strVCodeUrl);
		if(FAILED(hr)) return _T("");

		pHttpReq->SetRequestHeader(_T("Accept"), _T("*/*"));
		pHttpReq->SetRequestHeader(_T("Referer"), _T("http://reg.email.163.com/unireg/call.do?cmd=register.entrance&flow=m_main&from=m_wapreg"));
		pHttpReq->SetRequestHeader(_T("Accept-Language"), _T("zh-CN"));
		pHttpReq->SetRequestHeader(_T("User-Agent"), (LPCTSTR)strMobileAgents);
		pHttpReq->SetRequestHeader(_T("Host"), _T("reg.email.163.com"));
		pHttpReq->SetRequestHeader(_T("Connection"), _T("keep-alive"));

		hr = pHttpReq->Send();
		if(FAILED(hr)) return _T("");

		_variant_t varRspBody = pHttpReq->GetResponseBody();
		ULONG dataLen = varRspBody.parray->rgsabound[0].cElements;
		char *pContentBuffer = (char *)varRspBody.parray->pvData;

		_bstr_t cntType = pHttpReq->GetResponseHeader(_T("Content-Type"));
		_bstr_t imgType("image/jpeg");
		if (cntType != imgType) return _T("");

		CHAR szResult[MAX_PATH] = {0};
		long lRet = uu_recognizeByCodeTypeAndBytesA(pContentBuffer, dataLen, 2, szResult);
		strVCode = szResult;
		strVCode = strVCode.Right(strVCode.GetLength()-strVCode.ReverseFind('_')-1);
	}

	return strVCode;
}

int PostVCode2(IWinHttpRequestPtr& pHttpReq, CString strEnv, CString suspendId, CString strMail, HWND hParent)
{
	if (strEnv.IsEmpty() || suspendId.IsEmpty() || strMail.IsEmpty()) return 0;

__RETRY__:
	CString strEnv2 = strEnv, strTime2;
	BOOL bRet = GetEnvAndTimeString(strEnv2, strTime2);
	if (!bRet) return 0;

	CString strVCodeUrl2;
	strVCodeUrl2.Format(_T("http://reg.email.163.com/unireg/call.do?cmd=register.verifyCode&v=common/verifycode/vc_en&vt=grey&env=%s&t=%s"), strEnv2, strTime2);

	CString strVCode;
	if (hParent) {
		CImage *pImg = GetVerifyCode(pHttpReq, strVCodeUrl2);
		if(!pImg){
			MessageBox(hParent, _T("验证码获取错误！"), _T("提示"), MB_OK);
			return 0;
		}

		CVCodeDlg vcodeDlg(CWnd::FromHandle(hParent));
		vcodeDlg.m_pImage = pImg;
		INT_PTR nRet = vcodeDlg.DoModal();
		if (nRet != IDOK) return -1;

		strVCode = vcodeDlg.m_strVCode;
	}else{
		UINT dataLen = 0;
		char *pContentBuffer = GetVerifyCodeBuffer(pHttpReq, strVCodeUrl2, dataLen);
		if (!pContentBuffer){
			MessageBox(hParent, _T("验证码获取错误！"), _T("提示"), MB_OK);
			return 0;
		}

		CHAR szResult[MAX_PATH] = {0};
		long lRet = uu_recognizeByCodeTypeAndBytesA(pContentBuffer, dataLen, 2, szResult);
		strVCode = szResult;
		strVCode = strVCode.Right(strVCode.GetLength()-strVCode.ReverseFind('_')-1);
		delete [] pContentBuffer;
	}

	HRESULT hr = S_FALSE;
	hr = pHttpReq->Open(_T("POST"), _T("http://reg.email.163.com/unireg/call.do?cmd=register.resume"));
	if(FAILED(hr)) return 0;

	pHttpReq->SetRequestHeader(_T("Accept"), _T("*/*"));
	pHttpReq->SetRequestHeader(_T("Referer"), _T("http://reg.email.163.com/unireg/call.do?cmd=register.entrance&flow=m_main&from=m_wapreg"));
	pHttpReq->SetRequestHeader(_T("Accept-Language"), _T("zh-CN, en-US"));
	pHttpReq->SetRequestHeader(_T("User-Agent"), (LPCTSTR)strMobileAgents);
	pHttpReq->SetRequestHeader(_T("Host"), _T("reg.email.163.com"));
	pHttpReq->SetRequestHeader(_T("Connection"), _T("keep-alive"));
	pHttpReq->SetRequestHeader(_T("Origin"), _T("http://reg.email.163.com"));
	pHttpReq->SetRequestHeader(_T("X-Requested-With"), _T("XMLHttpRequest"));
	pHttpReq->SetRequestHeader(_T("Content-Type"), _T("application/x-www-form-urlencoded; charset=UTF-8"));

	CString strBody;
	strBody.Format(_T("vcode=%s&uid=%s&suspendId=%s"), strVCode, strMail, suspendId);

	hr = pHttpReq->Send((LPCTSTR)strBody);
	if(FAILED(hr)) return 0;

	long statusCode = pHttpReq->GetStatus();
	CString strContent;
	Get_WinHttp_RspString(pHttpReq, strContent);

	if (statusCode == 200 && strContent.Find(_T("VCODE_NOT_MATCH")) != -1){
		goto __RETRY__;
	}else if (statusCode == 200 && strContent.Find(_T("{\"code\":200}")) != -1){
		return 1;
	}

	return 0;
}

BOOL RegSubmit(IWinHttpRequestPtr& pHttpReq, CString strJSessionID, CString userString, CString mailType, CString passString, CString strVCode, CString& outContent, long& statusCode)
{
	if (strJSessionID.IsEmpty() || userString.IsEmpty() || mailType.IsEmpty() || passString.IsEmpty() || strVCode.IsEmpty()) return FALSE;

	CString strPostUrl;
	strPostUrl.Format(_T("https://ssl.mail.163.com/regall/unireg/call.do;jsessionid=%s?cmd=register.start"), strJSessionID);

	HRESULT hr = pHttpReq->Open(_T("POST"), (LPCTSTR)strPostUrl);
	if(FAILED(hr)) return FALSE;

	pHttpReq->SetRequestHeader(_T("Content-Type"), _T("application/x-www-form-urlencoded; charset=UTF-8"));
	pHttpReq->SetRequestHeader(_T("Referer"), _T("http://reg.email.163.com/unireg/call.do?cmd=register.entrance&flow=m_main&from=m_wapreg"));
	pHttpReq->SetRequestHeader(_T("Accept-Language"), _T("zh-CN, en-US"));
	pHttpReq->SetRequestHeader(_T("Origin"), _T("http://reg.email.163.com"));
	pHttpReq->SetRequestHeader(_T("User-Agent"), (LPCTSTR)strMobileAgents);
	pHttpReq->SetRequestHeader(_T("Host"), _T("ssl.mail.163.com"));
	pHttpReq->SetRequestHeader(_T("Connection"), _T("keep-alive"));
	pHttpReq->SetRequestHeader(_T("Accept"), _T("*/*"));

	CString strBody;
	strBody.Format(_T("name=%s&flow=m_main&uid=%s&password=%s&confirmPassword=%s&vcode=%s&from=m_wapreg&forcedFlow=m_main"), userString, userString+mailType, passString, passString, strVCode);

	hr = pHttpReq->Send((LPCTSTR)strBody);
	if(FAILED(hr)) return FALSE;

	statusCode = pHttpReq->GetStatus();
	Get_WinHttp_RspString(pHttpReq, outContent);

	return TRUE;
}

UINT RegThreadProc(LPVOID pParam)
{
	CThreadItem *pItem = (CThreadItem *)pParam;
	if (!pItem || !pItem->pThis) {
		AfxMessageBox(_T("参数错误，请退出软件重新开始！"));
		return FALSE;
	}

	CRegDlg *pMainDlg = pItem->pThis;
	int regCount = pItem->m_regCount;
	delete pItem;

	CoInitialize(NULL);

	try
	{
		while (regCount > 0)
		{
			HRESULT hr = S_FALSE;
			IWinHttpRequestPtr pHttpReq = NULL, pHttpReqSSL = NULL;
			if (!pHttpReq) {
				hr = pHttpReq.CreateInstance(__uuidof(WinHttpRequest));
				if(FAILED(hr)){
					pMainDlg->MessageBox(_T("Winhttp组建实例化错误！"), _T("提示"));
					pMainDlg->EnableControlls(TRUE);
					return FALSE;
				}
			}

			if (!pHttpReqSSL) {
				hr = pHttpReqSSL.CreateInstance(__uuidof(WinHttpRequest));
				if(FAILED(hr)){
					pMainDlg->MessageBox(_T("Winhttps组建实例化错误！"), _T("提示"));
					pMainDlg->EnableControlls(TRUE);
					return FALSE;
				}
			}

			COleVariant var(long(0x0080));
			pHttpReqSSL->PutOption(WinHttpRequestOption_SecureProtocols, var);

			HTTPREQUEST_PROXY_SETTING HTTPREQUEST_PROXYSETTING_DEFAULT   = 0;
			HTTPREQUEST_PROXY_SETTING HTTPREQUEST_PROXYSETTING_PRECONFIG = 0;
			HTTPREQUEST_PROXY_SETTING HTTPREQUEST_PROXYSETTING_DIRECT    = 1;
			HTTPREQUEST_PROXY_SETTING HTTPREQUEST_PROXYSETTING_PROXY     = 2;

			//hr = pHttpReqSSL->SetProxy(HTTPREQUEST_PROXYSETTING_PROXY, _T("127.0.0.1:8888"));
			//hr = pHttpReq->SetProxy(HTTPREQUEST_PROXYSETTING_PROXY, _T("127.0.0.1:8888"));

			bool bOK = jsManager.initGlobalContext();
			if(!bOK) {
				pMainDlg->MessageBox(_T("脚本引擎初始化错误！"), _T("提示"));
				pMainDlg->EnableControlls(TRUE);
				return FALSE;
			}

			jsval jsRet;
			bOK = jsManager.runScriptString(GetJSResBuffer(), jsRet);
			if(!bOK) {
				pMainDlg->MessageBox(_T("执行脚本错误！"), _T("提示"));
				pMainDlg->EnableControlls(TRUE);
				return FALSE;
			}

			CString strJSessionID, strEnv, strTime, strPrepareUrl;
			BOOL bRet = GetJSessionIDInfo(pHttpReq, strJSessionID, strEnv, strPrepareUrl);
			if (!bRet){
				pMainDlg->MessageBox(_T("网络参数获取错误！"), _T("提示"));
				pMainDlg->EnableControlls(TRUE);
				return FALSE;
			}

			CString strVCode;
			if (pMainDlg->m_bCheckAVCode) {
				UINT dataLen = 0;
				char *pContentBuffer = GetVCode1BufferByEnv(pHttpReq, strEnv, dataLen);
				if (!pContentBuffer){
					pMainDlg->MessageBox(_T("验证码获取错误！"), _T("提示"));
					pMainDlg->EnableControlls(TRUE);
					return FALSE;
				}

				CHAR szResult[MAX_PATH] = {0};
				long lRet = uu_recognizeByCodeTypeAndBytesA(pContentBuffer, dataLen, 2, szResult);
				strVCode = szResult;
				strVCode = strVCode.Right(strVCode.GetLength()-strVCode.ReverseFind('_')-1);
				delete [] pContentBuffer;

			}else{
				CImage *pImg = GetVCode1ImageByEnv(pHttpReq, strEnv);
				if(!pImg){
					pMainDlg->MessageBox(_T("验证码获取错误！"), _T("提示"));
					pMainDlg->EnableControlls(TRUE);
					return FALSE;
				}

				PreparePost(pHttpReqSSL, strPrepareUrl);

				CVCodeDlg vcodeDlg(pMainDlg);
				vcodeDlg.m_pImage = pImg;
				INT_PTR nRet = vcodeDlg.DoModal();
				if (nRet != IDOK){
					pMainDlg->MessageBox(_T("停止注册！"), _T("提示"));
					pMainDlg->EnableControlls(TRUE);
					return FALSE;
				}

				strVCode = vcodeDlg.m_strVCode;
			}

			CString strPre, strAfter;
			pMainDlg->GetDlgItemText(IDC_EDIT_PRE, strPre);
			pMainDlg->GetDlgItemText(IDC_EDIT_AFTER, strAfter);

			int iLeftRandLen = 17 - strPre.GetLength() - strAfter.GetLength();
			CString mailType(_T("%40163.com"));
			CString userString = strPre + GetRandomString(iLeftRandLen) + strAfter;
			//CString passString = GetRandomString(8);
			CString passString = _T("password123");


			CheckInfoPost(pHttpReq, userString, mailType);

			long statusCode = -1;
			CString strContent;

__VCODE_1_RETYR_:
			bRet = RegSubmit(pHttpReqSSL, strJSessionID, userString, mailType, passString, strVCode, strContent, statusCode);
			if (!bRet){
				pMainDlg->MessageBox(_T("提交注册信息错误！"), _T("提示"));
				pMainDlg->EnableControlls(TRUE);
				return FALSE;
			}

			if (statusCode == 200 && strContent.Find(_T("{\"code\":200}")) >= 0) { //注册成功
				pMainDlg->ShowMailItem(userString+_T("@163.com"), passString);
				--regCount;
				continue;
			}else
				if (statusCode == 200 && strContent.Find(_T("VCODE_NOT_MATCH")) >= 0) { //验证码不正确
					if (pMainDlg->m_bCheckAVCode) {
						strVCode = GetVCode1(pHttpReq, strEnv, NULL);
					}else{
						strVCode = GetVCode1(pHttpReq, strEnv, pMainDlg->GetSafeHwnd());
					}
					if (strVCode.IsEmpty()){
						pMainDlg->MessageBox(_T("验证码获取错误！"), _T("提示"));
						pMainDlg->EnableControlls(TRUE);
						return FALSE;
					}else if (strVCode == _T("####"))
					{
						pMainDlg->MessageBox(_T("停止注册！"), _T("提示"));
						pMainDlg->EnableControlls(TRUE);
						return FALSE;
					}
					goto __VCODE_1_RETYR_;
				}else
					if (statusCode == 201 && strContent.Find(_T("CaptchaForm")) >= 0) //二次验证码
					{
						int iRet = 0;
						CString strEnv2, strTime2, suspendId;
						strEnv2 = GetMidStrByLAndR(strContent, _T("\"envalue\" : \""), _T("\""));
						suspendId = GetMidStrByLAndR(strContent, _T("\"suspendId\" : \""), _T("\""));
						if (pMainDlg->m_bCheckAVCode) {
							iRet = PostVCode2(pHttpReq, strEnv2, suspendId, userString+mailType, NULL);
						}else{
							iRet = PostVCode2(pHttpReq, strEnv2, suspendId, userString+mailType, pMainDlg->GetSafeHwnd());
						}
						if (iRet == 1) { //注册成功
							pMainDlg->ShowMailItem(userString+_T("@163.com"), passString);
							--regCount;
							continue;
						}else if (iRet == -1)
						{
							pMainDlg->MessageBox(_T("停止注册！"), _T("提示"));
							pMainDlg->EnableControlls(TRUE);
							return FALSE;
						}
					}

					pMainDlg->MessageBox(_T("注册过程中遇到错误，重新注册中。。。"), _T("提示"));
		}
	}
	catch (...)
	{
		pMainDlg->MessageBox(_T("注册过程中遇到未知错误！"), _T("提示"));
		pMainDlg->EnableControlls(TRUE);
		return FALSE;
	}

	pMainDlg->MessageBox(_T("注册完成！"), _T("提示"));
	pMainDlg->EnableControlls(TRUE);

	return TRUE;
}