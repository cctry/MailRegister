﻿/////////////////////////////////////////////////////////////
// 声明：本源码来自 vc驿站：http://www.cctry.com
// C、C++、VC++ 各种学习资源，免费教程，期待您的加入！
/////////////////////////////////////////////////////////////
// RegDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MailRegister.h"
#include "RegDlg.h"
#include "afxdialogex.h"


// CRegDlg 对话框

IMPLEMENT_DYNAMIC(CRegDlg, CDialog)

CRegDlg::CRegDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRegDlg::IDD, pParent)
{
	m_bCheckAVCode = FALSE;
}

CRegDlg::~CRegDlg()
{
}

void CRegDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_MAIL, m_MailList);
	DDX_Control(pDX, IDC_CHECK_VCODE, m_useAutoVCode);
	DDX_Control(pDX, IDC_CHECK_AUTO_SAVE, m_AutoSaveFile);
}


BEGIN_MESSAGE_MAP(CRegDlg, CDialog)
	ON_BN_CLICKED(IDC_BTN_START, &CRegDlg::OnBnClickedBtnStart)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_MAIL, &CRegDlg::OnNMRClickListMail)
	ON_COMMAND(ID_MENU_COPY_ITEM, &CRegDlg::OnMenuCopyItem)
	ON_COMMAND(ID_MENU_EXP_LIST, &CRegDlg::OnMenuExpList)
	ON_BN_CLICKED(IDC_CHECK_VCODE, &CRegDlg::OnBnClickedCheckVcode)
	ON_BN_CLICKED(IDC_VCODE_TEST_BTN, &CRegDlg::OnBnClickedVcodeTestBtn)
END_MESSAGE_MAP()


// CRegDlg 消息处理程序


BOOL CRegDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  在此添加额外的初始化
	SetDlgItemText(IDC_EDIT_PRE, _T(""));
	SetDlgItemText(IDC_EDIT_AFTER, _T(""));
	SetDlgItemText(IDC_EDIT_RGE_COUNT, _T("1"));

	GetDlgItem(IDC_EDIT_AVCODE_USER)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDIT_AVCODE_PASS)->EnableWindow(FALSE);
	GetDlgItem(IDC_VCODE_TEST_BTN)->EnableWindow(FALSE);

	m_MailList.InsertColumn(0, _T("ID"), 0, 50);
	m_MailList.InsertColumn(1, _T("用户名"), 0, 200);
	m_MailList.InsertColumn(2, _T("密码"), 0, 160);

	m_MailList.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	//for (int idx = 0; idx < 10; ++idx)
	//{
	//	CString strU, strP;
	//	strU.Format(_T("User%03d"), idx);
	//	strP = _T("password");

	//	ShowMailItem(strU, strP);
	//}

	SetDlgItemText(IDC_EDIT_AVCODE_USER, _T("cctry.com"));
	SetDlgItemText(IDC_EDIT_AVCODE_PASS, _T("password"));

	m_AutoSaveFile.SetCheck(BST_CHECKED);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

#include "MailRegisterDlg.h"
void CRegDlg::SetStateInfo(LPCTSTR lpszText)
{
	if (!lpszText || _tcslen(lpszText) <= 0) return;
	CMailRegisterDlg *pParentDlg = (CMailRegisterDlg *)this->GetParent();
	ASSERT(pParentDlg);
	pParentDlg->SetStateInfo(lpszText);
}

CString GetTxtFilePath(CString strFileName)
{
	CString strPath;
	GetModuleFileName(NULL, strPath.GetBufferSetLength(MAX_PATH), MAX_PATH);
	strPath.ReleaseBuffer();

	strPath = strPath.Left(strPath.ReverseFind('\\')+1);

	return strPath + strFileName;
}

void CRegDlg::ShowMailItem(CString strMail, CString strPass)
{
	int nCount = m_MailList.GetItemCount();
	CString strIdx;
	strIdx.Format(_T("%d"), nCount);

	m_MailList.InsertItem(nCount, strIdx);
	m_MailList.SetItemText(nCount, 1, strMail);
	m_MailList.SetItemText(nCount, 2, strPass);

	int check = m_AutoSaveFile.GetCheck();
	if (check == BST_CHECKED) {
		CString path = GetTxtFilePath(_T("mail.txt"));
		CStdioFile mFile;
		BOOL bRet = mFile.Open(path, CFile::modeCreate|CFile::modeNoTruncate|CFile::modeWrite);
		if (!bRet) return;
		mFile.SeekToEnd();
		mFile.WriteString(strMail+_T("||")+strPass);
		mFile.Close();
	}
}


#include "RegThread.h"
void CRegDlg::OnBnClickedBtnStart()
{
	CString strPre, strAfter;
	GetDlgItemText(IDC_EDIT_PRE, strPre);
	GetDlgItemText(IDC_EDIT_AFTER, strAfter);
	UINT regCount = GetDlgItemInt(IDC_EDIT_RGE_COUNT);

	if (strPre.GetLength() > 4 || strPre[0] >= '0' && strPre[0] <= '9')
	{
		MessageBox(_T("前缀字符串的长度应小于等于4，并且以字母开头！"), _T("提示"));
		return;
	}

	if (strAfter.GetLength() > 4)
	{
		MessageBox(_T("后缀字符串的长度应小于等于4个字符！"), _T("提示"));
		return;
	}

	if (regCount <= 0 || regCount > 999999)
	{
		MessageBox(_T("注册邮箱个数不合法，请介于 1 到 999999 之间！"), _T("提示"));
		return;
	}

	CThreadItem *pItem = new CThreadItem(this, regCount);
	CWinThread *pThread = AfxBeginThread(RegThreadProc, pItem);
	if (pThread) {
		EnableControlls(FALSE);
	}
}


void CRegDlg::OnNMRClickListMail(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	int iSelIdx = pNMItemActivate->iItem;
	if (iSelIdx < 0) return;

	int listCount = m_MailList.GetItemCount();
	if (listCount <= 0) return;

	CMenu mMenu, *pMenu = NULL;
	mMenu.LoadMenu(IDR_MENU1);
	pMenu = mMenu.GetSubMenu(0);

	CPoint pt;
	GetCursorPos(&pt);
	pMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, pt.x, pt.y, this);

	*pResult = 0;
}


void CRegDlg::OnMenuCopyItem()
{
	CString strResult;
	int iSelIdx = m_MailList.GetSelectionMark();
	strResult = m_MailList.GetItemText(iSelIdx, 1);
	strResult = strResult + _T("||") + m_MailList.GetItemText(iSelIdx, 2);

	int nLen = strResult.GetLength()*sizeof(TCHAR);

	do 
	{
		if(!OpenClipboard()) break;
		EmptyClipboard();
		HGLOBAL pClipBuffer = GlobalAlloc(GMEM_DDESHARE, nLen+2);
		if (!pClipBuffer) break;

		LPVOID pBuffer = GlobalLock(pClipBuffer);
		memcpy_s(pBuffer, nLen+1, (LPCTSTR)strResult, nLen);
		GlobalUnlock(pClipBuffer);
		HANDLE handle = SetClipboardData(CF_UNICODETEXT, pClipBuffer);
		if (!handle) break;

		CloseClipboard();
		return;

	} while (0);

	MessageBox(_T("拷贝失败！"), _T("提示"));
}


void CRegDlg::OnMenuExpList()
{
	int listCount = m_MailList.GetItemCount();
	if (listCount <= 0) return;

	CFileDialog dlg(FALSE, _T("Txt"), _T("Mail"), OFN_OVERWRITEPROMPT|OFN_HIDEREADONLY, _T("文本文件(*.txt)|*.txt||"), this);
	dlg.m_ofn.lpstrTitle = _T("保存邮件列表");
	if (dlg.DoModal() != IDOK) return;

	CString filePath = dlg.GetPathName();
	CStdioFile mFile;
	BOOL bRet = mFile.Open(filePath, CFile::modeCreate|CFile::modeNoTruncate|CFile::modeWrite);
	if (!bRet){
		MessageBox(_T("保存文件失败！"), _T("提示"));
		return;
	}

	mFile.SeekToEnd();
	CString strResult;
	for (int idx = 0; idx < listCount; ++idx)
	{
		strResult = m_MailList.GetItemText(idx, 1);
		strResult = strResult + _T("||") + m_MailList.GetItemText(idx, 2) + _T("\r\n");
		mFile.WriteString(strResult);
	}

	mFile.Close();
	MessageBox(_T("保存成功！\n\n路径：")+filePath, _T("提示"));
}


void CRegDlg::OnBnClickedCheckVcode()
{
	int check = m_useAutoVCode.GetCheck();
	if (check == BST_CHECKED) {
		GetDlgItem(IDC_EDIT_AVCODE_USER)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_AVCODE_PASS)->EnableWindow(TRUE);
		GetDlgItem(IDC_VCODE_TEST_BTN)->EnableWindow(TRUE);
		m_bCheckAVCode = TRUE;
	}else if (check == BST_UNCHECKED) {
		GetDlgItem(IDC_EDIT_AVCODE_USER)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_AVCODE_PASS)->EnableWindow(FALSE);
		GetDlgItem(IDC_VCODE_TEST_BTN)->EnableWindow(FALSE);
		m_bCheckAVCode = FALSE;
	}
}

void CRegDlg::OnBnClickedVcodeTestBtn()
{
	CString avCodeUser, avCodePass;
	GetDlgItemText(IDC_EDIT_AVCODE_USER, avCodeUser);
	GetDlgItemText(IDC_EDIT_AVCODE_PASS, avCodePass);
	if (avCodeUser.IsEmpty() || avCodePass.IsEmpty()){
		MessageBox(_T("请输入UU打码平台的用户名和密码！"), _T("提示"));
		return;
	}

	long lRet = -1;
	uu_setSoftInfoW(105880, _T("e14b9fdcdb3e47fc9823c8ec24ad8e17"));
	lRet = uu_loginW(avCodeUser, avCodePass);
	switch(lRet)
	{
	case -11002:
		MessageBox(_T("用户名或密码错误！"), _T("提示"));
		return;
	case -11003:
		MessageBox(_T("账户被锁定！"), _T("提示"));
		return;
	case -11006:
		MessageBox(_T("账户登录过于频繁！"), _T("提示"));
		return;
	case -11007:
		MessageBox(_T("当前IP被禁止登录！"), _T("提示"));
		return;
	case -11009:
		MessageBox(_T("余额不足！"), _T("提示"));
		return;
	case -11010:
		MessageBox(_T("帐户异常！"), _T("提示"));
		return;
	}

	if (lRet < 0) {
		MessageBox(_T("测试失败！"), _T("提示"));
		return;
	}else{
		//HMODULE hInstance = GetModuleHandle(NULL);
		//HRSRC hRes = FindResource(hInstance, MAKEINTRESOURCE(IDR_JPG_VCODE), _T("JPG"));
		//if(NULL == hRes) return;

		//HGLOBAL hgpt = LoadResource(hInstance, hRes);
		//if (NULL == hgpt) return;

		//DWORD rcSize = SizeofResource(hInstance, hRes);
		//char *lpBuff = (char*)LockResource(hgpt);

		//lRet = uu_recognizeByCodeTypeAndBytesA(lpBuff, rcSize, 5, szResult);
		//retString = strResult.Right(strResult.GetLength()-strResult.ReverseFind('_')-1);

		CString tmpPath;
		GetTempPath(MAX_PATH, tmpPath.GetBufferSetLength(MAX_PATH));
		tmpPath.ReleaseBuffer();
		tmpPath += _T("9824.png");

		HMODULE hInstance = GetModuleHandle(NULL);
		HRSRC hRes = FindResource(hInstance, MAKEINTRESOURCE(IDR_JPG_VCODE), _T("JPG"));
		if(NULL == hRes) return;

		HGLOBAL hgpt = LoadResource(hInstance, hRes);
		if (NULL == hgpt) return;

		DWORD rcSize = SizeofResource(hInstance, hRes);
		char *lpBuff = (char*)LockResource(hgpt);

		CFile imgFile;
		BOOL bRet = imgFile.Open(tmpPath, CFile::modeCreate|CFile::modeWrite);
		if (!bRet) return;
		imgFile.Write(lpBuff, rcSize);
		imgFile.Close();

		TCHAR szResult[MAX_PATH] = {0};
		lRet = uu_recognizeByCodeTypeAndPathW(tmpPath, 5, szResult);

		CString strResult(szResult);
		strResult = strResult.Right(4);
		if (strResult == _T("9824")) {
			MessageBox(_T("测试成功！"), _T("提示"));
		}else{
			MessageBox(_T("测试失败！"), _T("提示"));
		}
	}
}

void CRegDlg::EnableControlls(BOOL bEnable)
{
	GetDlgItem(IDC_EDIT_PRE)->EnableWindow(bEnable);
	GetDlgItem(IDC_EDIT_AFTER)->EnableWindow(bEnable);
	GetDlgItem(IDC_EDIT_RGE_COUNT)->EnableWindow(bEnable);
	GetDlgItem(IDC_CHECK_VCODE)->EnableWindow(bEnable);
	GetDlgItem(IDC_EDIT_AVCODE_USER)->EnableWindow(bEnable);
	GetDlgItem(IDC_EDIT_AVCODE_PASS)->EnableWindow(bEnable);
	GetDlgItem(IDC_VCODE_TEST_BTN)->EnableWindow(bEnable);
	GetDlgItem(IDC_CHECK_AUTO_SAVE)->EnableWindow(bEnable);

	GetDlgItem(IDC_BTN_START)->EnableWindow(bEnable);
	GetDlgItem(IDC_LIST_MAIL)->EnableWindow(bEnable);
}