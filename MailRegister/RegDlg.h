/////////////////////////////////////////////////////////////
// 声明：本源码来自 vc驿站：http://www.cctry.com
// C、C++、VC++ 各种学习资源，免费教程，期待您的加入！
/////////////////////////////////////////////////////////////

#pragma once
#include "resource.h"
#include "afxwin.h"


// CRegDlg 对话框

class CRegDlg : public CDialog
{
	DECLARE_DYNAMIC(CRegDlg)

public:
	CRegDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CRegDlg();

// 对话框数据
	enum { IDD = IDD_REG_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();

public:
	CListCtrl m_MailList;
	afx_msg void OnBnClickedBtnStart();
	void ShowMailItem(CString strMail, CString strPass);
	afx_msg void OnNMRClickListMail(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnMenuCopyItem();
	afx_msg void OnMenuExpList();
	afx_msg void OnBnClickedCheckVcode();
	CButton m_useAutoVCode;
	void SetStateInfo(LPCTSTR lpszText);
	afx_msg void OnBnClickedVcodeTestBtn();
	BOOL m_bCheckAVCode;
	CButton m_AutoSaveFile;

	void EnableControlls(BOOL bEnable);
};
