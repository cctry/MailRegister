/////////////////////////////////////////////////////////////
// 声明：本源码来自 vc驿站：http://www.cctry.com
// C、C++、VC++ 各种学习资源，免费教程，期待您的加入！
/////////////////////////////////////////////////////////////

#pragma once


// CUsageDlg 对话框

class CUsageDlg : public CDialog
{
	DECLARE_DYNAMIC(CUsageDlg)

public:
	CUsageDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CUsageDlg();

// 对话框数据
	enum { IDD = IDD_USAGE_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
};
