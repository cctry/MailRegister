/////////////////////////////////////////////////////////////
// 声明：本源码来自 vc驿站：http://www.cctry.com
// C、C++、VC++ 各种学习资源，免费教程，期待您的加入！
/////////////////////////////////////////////////////////////


// CCTryDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MailRegister.h"
#include "CCTryDlg.h"
#include "afxdialogex.h"


// CCCTryDlg 对话框

IMPLEMENT_DYNAMIC(CCCTryDlg, CDialog)

CCCTryDlg::CCCTryDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCCTryDlg::IDD, pParent)
{

}

CCCTryDlg::~CCCTryDlg()
{
}

void CCCTryDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EXPLORER1, m_Explorer);
}


BEGIN_MESSAGE_MAP(CCCTryDlg, CDialog)
END_MESSAGE_MAP()


// CCCTryDlg 消息处理程序


BOOL CCCTryDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	_variant_t varUrl = _T("http://www.cctry.com/?from=mailreger");

	m_Explorer.put_Silent(TRUE);
	m_Explorer.Navigate2(&varUrl, NULL, NULL, NULL, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}
