/////////////////////////////////////////////////////////////
// 声明：本源码来自 vc驿站：http://www.cctry.com
// C、C++、VC++ 各种学习资源，免费教程，期待您的加入！
/////////////////////////////////////////////////////////////

#pragma once
#include "resource.h"

// CVCodeDlg 对话框

class CVCodeDlg : public CDialog
{
	DECLARE_DYNAMIC(CVCodeDlg)

public:
	CVCodeDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CVCodeDlg();

// 对话框数据
	enum { IDD = IDD_VCODE_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CImage *m_pImage;
	CString m_strVCode;
	afx_msg void OnBnClickedOk();
	virtual void OnCancel();
};
