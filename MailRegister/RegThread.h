/////////////////////////////////////////////////////////////
// 声明：本源码来自 vc驿站：http://www.cctry.com
// C、C++、VC++ 各种学习资源，免费教程，期待您的加入！
/////////////////////////////////////////////////////////////

#pragma once

#include "cdef.h"
#pragma comment(lib, "./UUWiseHelper.lib")

UINT RegThreadProc(LPVOID pParam);

#define Get_WinHttp_RspString(IWinHttpRequestPtr_Obj, String_Obj){				\
	_variant_t varRspBody = IWinHttpRequestPtr_Obj->GetResponseBody();		\
	ULONG dataLen = varRspBody.parray->rgsabound[0].cElements;				\
	char *pContentBuffer = (char *)varRspBody.parray->pvData;				\
	String_Obj = pContentBuffer;}

inline CString GetMidStrByLAndR(CString& strSrc, CString strLeft, CString strRight, BOOL bIncludeStart = FALSE, BOOL bIncludeEnd = FALSE);

class CRegDlg;
class CThreadItem {
public:
	CRegDlg *pThis;
	UINT m_regCount;

	CThreadItem(){
		pThis = NULL;
		m_regCount = 0;
	}

	CThreadItem(CRegDlg *pDlg, UINT regCount){
		pThis = pDlg;
		m_regCount = regCount;
	}
};