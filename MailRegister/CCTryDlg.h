/////////////////////////////////////////////////////////////
// 声明：本源码来自 vc驿站：http://www.cctry.com
// C、C++、VC++ 各种学习资源，免费教程，期待您的加入！
/////////////////////////////////////////////////////////////

#pragma once
#include "explorer1.h"


// CCCTryDlg 对话框

class CCCTryDlg : public CDialog
{
	DECLARE_DYNAMIC(CCCTryDlg)

public:
	CCCTryDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CCCTryDlg();

// 对话框数据
	enum { IDD = IDD_CCTRY_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CExplorer1 m_Explorer;
	virtual BOOL OnInitDialog();
};
