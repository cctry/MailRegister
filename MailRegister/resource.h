//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MailRegister.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_MAILREGISTER_DIALOG         102
#define IDR_MAINFRAME                   128
#define IDD_REG_DIALOG                  129
#define IDD_USAGE_DIALOG                130
#define IDD_CCTRY_DIALOG                131
#define IDR_MENU1                       132
#define IDD_VCODE_DIALOG                133
#define IDR_JPG_VCODE                   134
#define IDR_JSCRIPT                     135
#define IDC_TAB1                        1000
#define IDC_TAB                         1000
#define IDC_LIST_MAIL                   1001
#define IDC_EDIT_PRE                    1002
#define IDC_EDIT_AFTER                  1003
#define IDC_BTN_VCODE                   1004
#define IDC_CHECK1                      1004
#define IDC_CHECK_AUTO_SAVE             1004
#define IDC_BTN_START                   1005
#define IDC_EXPLORER1                   1005
#define IDC_MFCLINK1                    1006
#define IDC_EDIT_RGE_COUNT              1007
#define IDC_MFCLINK2                    1007
#define IDC_CHECK_VCODE                 1008
#define IDC_EDIT_AVCODE_USER            1009
#define IDC_EDIT_AVCODE_PASS            1010
#define IDC_VCODE_TEST_BTN              1011
#define IDC_EDIT_VCODE                  1012
#define IDC_VCODE_IMAGE                 1013
#define ID_MENU_COPY_ITEM               32773
#define ID_MENU_EXP_LIST                32774

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
