/////////////////////////////////////////////////////////////
// 声明：本源码来自 vc驿站：http://www.cctry.com
// C、C++、VC++ 各种学习资源，免费教程，期待您的加入！
/////////////////////////////////////////////////////////////

// VCodeDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "VCodeDlg.h"
#include "afxdialogex.h"


// CVCodeDlg 对话框

IMPLEMENT_DYNAMIC(CVCodeDlg, CDialog)

CVCodeDlg::CVCodeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CVCodeDlg::IDD, pParent)
{
	m_pImage = NULL;
}

CVCodeDlg::~CVCodeDlg()
{
}

void CVCodeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CVCodeDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CVCodeDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CVCodeDlg 消息处理程序


BOOL CVCodeDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CStatic *pStaticPic = (CStatic *)GetDlgItem(IDC_VCODE_IMAGE);
	if (pStaticPic && (HBITMAP)(*m_pImage)) {
		HBITMAP retBitmap = pStaticPic->SetBitmap((HBITMAP)(*m_pImage));
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}


void CVCodeDlg::OnBnClickedOk()
{
	GetDlgItemText(IDC_EDIT_VCODE, m_strVCode);
	if (m_strVCode.IsEmpty() || m_strVCode.GetLength() > 20) {
		MessageBox(_T("请输入正确的验证码！"), _T("提示"));
		return;
	}
	CDialog::OnOK();
}


void CVCodeDlg::OnCancel()
{
	int iRet = MessageBox(_T("确定取消验证码输入，停止注册吗？"), _T("提示"), MB_OKCANCEL);
	if (iRet != IDOK) return;

	CDialog::OnCancel();
}
