##MailRegister是什么?

项目简介：
本工程是用C++实现对网易163邮箱的全自动注册工作，具体的功能包括：
1. 全自动随机生成邮箱账号；
2. 为了避免与已有账号重复，可以自定义添加账号的前缀与后缀；
3. 自定义设定注册邮箱的数量；
4. 防止异常发生程序自动退出，可实时保存注册结果到文件中；
5. 打码平台自动测试与对接；
6. 注册结果右键菜单选项的复制与导出。

本库为`开源库`。由 [VC驿站](http://www.cctry.com/ "打开VC驿站论坛") 整理发布。[打开原帖](http://www.cctry.com/thread-255312-1-1.html)


开源地址：[https://git.oschina.net/cctry/MailRegister](https://git.oschina.net/cctry/MailRegister)
